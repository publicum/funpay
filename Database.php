<?php

namespace FpDbTest;

use Exception;
use mysqli;

class Database implements DatabaseInterface
{
    private mysqli $mysqli;
	
	private $err_messages = [
		'TPL_ERR_PARAM_AMOUNT' => 'Template error. The number of input parameters does not match the number of specifiers in the template',
		'PARAM_ERR_UNSUP_TYPE' => 'Parameter error. The type of the passed parameter is not supported',
		'PARAM_ERR_TYPE_MISMATCH' => 'Parameter error. The type of the passed parameter does not match the specifier',
		'TPL_ERR_COND_BLOCK' => 'Template error. Conditional block is set incorrectly',
	];
	
	private $special_value = '/*SpecVal*/';

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }
	
	public function buildQuery(string $query, array $args = []): string
    {
        /*** Подстановка аргументов ***/
		$q_parts = explode('?', $query);
		
		if (count($args) !== count($q_parts) - 1) {
			throw new Exception($this->err_messages['TPL_ERR_PARAM_AMOUNT']);
		}
		
		$query_final = $q_parts[0];
		$arg_idx = 0;
		$has_spec = true; // признак наличия спецификатора
		
		for ($i = 1; $i < count($q_parts); ++$i) {
			$arg = $args[$arg_idx];
			$spec = substr($q_parts[$i], 0, 1);
			
			if ($arg === $this->special_value) {
				$query_final .= $this->special_value;
				
				if (!in_array($spec, ['d', 'f', '#', 'a'])) {
					$has_spec = false;
				}
			}
			elseif ($spec == 'd') {
				if (is_null($arg)) {
					$query_final .= 'NULL';
				}
				else {
					$arg = (int)$arg;
					$query_final .= $arg;
				}
			}
			elseif ($spec == 'f') {
				if (is_null($arg)) {
					$query_final .= 'NULL';
				}
				else {
					$arg = (float)$arg;
					$query_final .= $arg;
				}
			}
			elseif ($spec == '#') {
				if (is_array($arg)) {
					$query_final .= implode(', ', array_map( function ($val) {
						return is_string($val)
							? '`' . addslashes($val) . '`'
							: '`' . addslashes(strval($val)) . '`';
					}, $arg));
				}
				elseif (is_string($arg)) {
					$query_final .= '`' . addslashes($arg) . '`';
				}
				else {
					$query_final .= '`' . addslashes(strval($arg)) . '`';
				}
			}
			elseif ($spec == 'a') {
				if (is_array($arg)) {
					// Ассоциативный массив
					if (array_values($arg) !== $arg) {
						$key_val_pairs = [];
						
						foreach ($arg as $key => $value) {
							$key_val_pairs[] = '`' . addslashes(strval($key)) . '`' . ' = ' . $this->formatParam($value);
						}
						
						$query_final .= implode(', ', $key_val_pairs);
					}
					// Список
					else {
						$query_final .= implode(', ', array_map( function ($val) {
							return $this->formatParam($val);
						}, $arg));
					}
				}
				else {
					throw new Exception($this->err_messages['PARAM_ERR_TYPE_MISMATCH']);
				}
			}
			else {
				$query_final .= $this->formatParam($arg) . ' ';
				$has_spec = false;
			}
			
			$query_final .= $has_spec ? substr($q_parts[$i], 1) : $q_parts[$i];
			++$arg_idx;
		}
		/*** Конец ***/
		
		/*** Проверка наличия и правильности условных блоков ***/
		if (!preg_match('/[{}]/', $query_final)) {
			return $query_final;
		}
		
		$open_brace_cnt = substr_count($query_final, '{');
		$close_brace_cnt = substr_count($query_final, '}');
		
		if ($open_brace_cnt !== $close_brace_cnt ) {
			throw new Exception($this->err_messages['TPL_ERR_COND_BLOCK']);
		}
		
		$q_parts = explode('{', $query_final);
		
		if (strpos($q_parts[0], '}')) {
			throw new Exception($this->err_messages['TPL_ERR_COND_BLOCK']);
		}
		
		for ($i = 1; $i < count($q_parts); ++$i) {
			if (!strpos($q_parts[$i], '}')) {
				throw new Exception($this->err_messages['TPL_ERR_COND_BLOCK']);
			}
		}
		/*** Конец ***/
		
		/*** Включение/исключение условных блоков ***/
		$query_final = $q_parts[0];
		
		for ($i = 1; $i < count($q_parts); ++$i) {
			$parts = explode('}', $q_parts[$i]);
			
			if (!strpos($parts[0], $this->special_value)) {
				$query_final .= $parts[0];
			}
			
			$query_final .= $parts[1];
		}
		/*** Конец ***/
		
		return $query_final;
    }

    public function skip()
    {
        return $this->special_value;
		
		//throw new Exception();
    }
	
	private function formatParam($param)
	{
		if (is_string($param)) {
			return '\'' . addslashes($param) . '\'';
		}
		elseif (is_int($param)) {
			return $param;
		}
		elseif (is_float($param)) {
			return $param;
		}
		elseif (is_bool($param)) {
			return $param ? 1 : 0;
		}
		elseif (is_null($param)) {
			return 'NULL';
		}
		else {
			throw new Exception($this->err_messages['PARAM_ERR_UNSUP_TYPE']);
		}
	}
}
